from owlready2 import get_ontology
from owlready2 import *
import psycopg2

DB_HOST = "localhost"
DB_NAME = "db_ventas"
DB_USER = "postgres"
DB_PASS = "12345"

conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"
onto = get_ontology("file://" + ruta_ontologia).load()

# Diccionario para almacenar las subpropiedades de cada propiedad de datos
properties_dict = {}

# Recorre las Data Properties
for data_prop in onto.data_properties():
    # Obtiene todas las subpropiedades (subclases) de la propiedad de datos actual
    sub_properties = [sub_prop.name for sub_prop in data_prop.subclasses() if sub_prop != data_prop]
    
    # Agrega la propiedad de datos y sus subpropiedades al diccionario
    if sub_properties:
        properties_dict[data_prop.name] = sub_properties
   
def get_sql_type(owl_type):
    # Mapeo de tipos de datos de Owlready2 a tipos de datos de PostgreSQL
    if owl_type == str or owl_type == int or owl_type == float:
        return "VARCHAR(255)" if owl_type == str else "INTEGER" if owl_type == int else "REAL"
    # Agrega más mapeos según sea necesario

def get_primary_key(table_name):
    # Obtener las propiedades de datos asociadas a la clase
    #¿COMO HALLO LOS DATA PROPERTIES DE UNA TABLA?
    data_properties = []
    for prop in onto.data_properties():
        if prop.domain[0] == table_name:
            data_properties.append(prop.name)
    # Verificar si alguna de las claves del diccionario está presente en las propiedades de datos
    for key in properties_dict.keys():
        if key in data_properties:
            return key  # Devolver la primera clave del diccionario que esté presente en las propiedades de datos
    
    # Si ninguna clave del diccionario está presente en las propiedades de datos, devolver None
    return None

# Crear las tablas
def crear_tablas():
    print("Generación de Sentencias SQL para Crear Tablas:")
    for clase in onto.classes():
        print(f"\n-- Tabla para la clase: {clase.name}")
        
        # Obtener las data properties asociadas a la clase
        data_properties = [(prop.name, prop.range[0]) for prop in onto.data_properties() if prop.domain[0] == clase]

        # Obtener la clave primaria de la tabla
        print(clase.name)
        primary_key = get_primary_key(clase)
        
        if primary_key:
            # Crear la sentencia SQL
            c = f'''
                CREATE TABLE {clase.name} (
                    {', '.join([f'{atributo} {get_sql_type(tipo)}' for atributo, tipo in data_properties])},
                    PRIMARY KEY ({primary_key})
                );
            '''
        else:
            print("No se pudo encontrar una clave primaria para esta tabla.")
        
        with conn as conexion:
            with conexion.cursor() as cursor:
                cursor.execute(c)
        
        conexion.commit()

# Ejecutar la función para crear las tablas
crear_tablas()



