#Utilizando libreria owlready2 para ontologias
from owlready2 import get_ontology
from owlready2 import *
#pip install psycopg2 
import psycopg2
import psycopg2.extras


#Conexion con PostgreSQL
DB_HOST = "localhost"
DB_NAME = "db_ventas"
DB_USER = "postgres"
DB_PASS = "12345"

conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"
# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

def get_sql_type(owl_type):
    # Mapeo de tipos de datos de Owlready2 a tipos de datos de PostgreSQL
    if owl_type == str or owl_type == int or owl_type == float:
        return "VARCHAR(255)" if owl_type == str else "INTEGER" if owl_type == int else "REAL"
    # Agrega más mapeos según sea necesario

def crear_tabla():
    print("Generación de Sentencias SQL para Crear Tablas:")
    for clase in onto.classes():
        print(f"\n-- Tabla para la clase: {clase.name}")
        
        # Obtener las data properties asociadas a la clase
        data_properties = [(prop.name, get_sql_type(prop.range[0])) for prop in onto.data_properties() if prop.domain[0] == clase]

        # Crear la sentencia SQL
        c = f'''
            CREATE TABLE {clase.name} (
                {', '.join([f'{atributo} {tipo}' for atributo, tipo in data_properties])},
                PRIMARY KEY ({data_properties[0][0]})
            );
        '''
        with conn as conexion:
            with conexion.cursor() as cursor:
                cursor.execute(c)
        
        conexion.commit()

# Solo debería ejecutarse una vez para crear las tablas utilizando la ontología      
crear_tabla()

