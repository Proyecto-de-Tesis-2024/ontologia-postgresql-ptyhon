from owlready2 import get_ontology
from owlready2 import *
import psycopg2

DB_HOST = "localhost"
DB_NAME = "db_ventas"
DB_USER = "postgres"
DB_PASS = "12345"

conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"
onto = get_ontology("file://" + ruta_ontologia).load()

#retorna un diccionario --> clave: primarykey, valor: lista de data properties
def get_properties_dic():
    # Diccionario para almacenar las subpropiedades de cada propiedad de datos
    properties_dict = {}
    # Recorre las Data Properties
    for data_prop in onto.data_properties():
        # Obtiene todas las subpropiedades (subclases) de la propiedad de datos actual
        sub_properties = [sub_prop.name for sub_prop in data_prop.subclasses() if sub_prop != data_prop]
        
        # Agrega la propiedad de datos y sus subpropiedades al diccionario
        if sub_properties:
            properties_dict[data_prop.name] = sub_properties
    return properties_dict   

#obtiene los tipos de datos de las data properties
def get_sql_type(owl_type):
    # Mapeo de tipos de datos de Owlready2 a tipos de datos de PostgreSQL
    if owl_type == str or owl_type == int or owl_type == float:
        return "VARCHAR(255)" if owl_type == str else "INTEGER" if owl_type == int else "REAL"

#Obtiene PRIMARY KEY de cada TABLA
def get_primary_key(table_name):
    # Obtener las propiedades de datos asociadas a la clase
    data_properties = []
    for prop in onto.data_properties():
        if prop.domain[0] == table_name:
            data_properties.append(prop.name)
    # Verificar si alguna de las claves del diccionario está presente en las propiedades de datos
    for key in get_properties_dic.keys():
        if key in data_properties:
            return key  # Devolver la CLAVE PRIMARIA del diccionario que esté presente en las propiedades de datos

def tabla_existe(table_name):
    print(f"Verificando si la tabla {table_name} existe...")
    with conn as conexion:
        with conexion.cursor() as cursor:
            # Convertir ambos nombres a minúsculas antes de comparar
            lower_table_name = table_name.lower()
            cursor.execute("SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE lower(table_name) = %s)", (lower_table_name,))
            exists = cursor.fetchone()[0]
            print(f"Tabla {table_name} existe: {exists}")
            return exists

def crear_tablas():
    print("Generación de Sentencias SQL para Crear Tablas:")
    for clase in onto.classes():
        table_name = clase.name
        print(f"\n-- Tabla para la clase: {table_name}")

        if not tabla_existe(table_name):
            # Solo crear la tabla si no existe
            primary_key = get_primary_key(clase)
            
            if primary_key:
                # Construir la sentencia SQL para crear la tabla
                data_properties = [(prop.name, prop.range[0]) for prop in onto.data_properties() if prop.domain[0] == clase]
                column_definitions = [f'{atributo} {get_sql_type(tipo)}' for atributo, tipo in data_properties]
                c = f'''
                    CREATE TABLE {table_name} (
                        {', '.join(column_definitions)},
                        PRIMARY KEY ({primary_key})
                    );
                '''
                print(f'Se agrego correctamente la Tabla {table_name}')
                # Ejecutar la sentencia SQL
                with conn as conexion:
                    with conexion.cursor() as cursor:
                        cursor.execute(c)
            else:
                print("No se pudo encontrar una clave primaria para esta tabla.")
        else:
            print(f"La tabla {table_name} ya existe.")

        # Obtener las columnas existentes en la tabla
        existing_columns = []
        with conn as conexion:
            with conexion.cursor() as cursor:
                cursor.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = '{table_name.lower()}';")
                existing_columns = [row[0] for row in cursor.fetchall()]
        print(existing_columns)

        # Verificar si hay nuevas columnas para agregar a la tabla
        for prop in onto.data_properties():
            if prop.domain[0] == clase and prop.name not in existing_columns:
                # Verificar si la columna ya existe en la tabla
                if prop.name.lower() not in existing_columns:
                    # Agregar la nueva columna a la tabla
                    with conn as conexion:
                        with conexion.cursor() as cursor:
                            cursor.execute(f'ALTER TABLE {table_name} ADD COLUMN {prop.name} {get_sql_type(prop.range[0])};')
                            print(f'Se agrego correctamente la columna {prop.name}')
                else:
                    print(f"La columna {prop.name} ya existe en la tabla {table_name}.")



# Ejecutar la función para crear las tablas
crear_tablas()



