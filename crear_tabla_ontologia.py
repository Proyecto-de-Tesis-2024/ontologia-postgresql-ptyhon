from owlready2 import *
import psycopg2

# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontology-university.owx"

# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

# Utilizar un cursor
conexion = psycopg2.connect(user='postgres', 
                            password='12345',
                            host='127.0.0.1', 
                            port='5432', 
                            database='db_personas')
cursor = conexion.cursor()

# Obtener información de la clase Materia
clase_materia = onto.Materia
atributos_materia = [dp.name for dp in onto.data_properties() if dp.domain[0] == clase_materia]
print(atributos_materia)

# Crear la tabla Materia dinámicamente
sql_creacion_tabla = f'''
    CREATE TABLE {clase_materia.name} (
        {', '.join([f'{atributo} VARCHAR(255)' for atributo in atributos_materia])},
        PRIMARY KEY ({atributos_materia[0]})
    );
'''
cursor.execute(sql_creacion_tabla)

# Insertar datos en la tabla Materia (ejemplo)
sql_insercion_datos = f'''
    INSERT INTO {clase_materia.name} ({', '.join(atributos_materia)})
    VALUES ({', '.join(['%s' for _ in atributos_materia])});
'''

# Puedes insertar datos utilizando información de la ontología
datos_materia = [
    ('IN001', 4, 'Matematica'),
    ('CO002', 5, 'Electronica'),
    ('ME003', 4, 'Comunicacion')
]

cursor.executemany(sql_insercion_datos, datos_materia)

# Confirmar la transacción
conexion.commit()
print("Tabla creada exitosamente.")
# Cerrar cursor y conexión
cursor.close()
conexion.close()
