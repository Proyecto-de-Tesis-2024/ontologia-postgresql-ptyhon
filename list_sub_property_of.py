from owlready2 import get_ontology
from owlready2 import *
# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"

# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

# Recorre las Data Properties
for data_prop in onto.data_properties():
    print(f"Data Property: {data_prop.name}")
    
    # Recorre las propiedades de las que es subpropiedad (SubPropertyOf)
    for sub_prop in data_prop.is_a:
        if sub_prop in onto.data_properties():
            print(f"  SubPropertyOf: {sub_prop.name}")