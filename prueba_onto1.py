from owlready2 import get_ontology
import psycopg2

DB_HOST = "localhost"
DB_NAME = "db_ventas"
DB_USER = "postgres"
DB_PASS = "12345"

conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"
onto = get_ontology("file://" + ruta_ontologia).load()

def get_sql_type(owl_type):
    # Mapeo de tipos de datos de Owlready2 a tipos de datos de PostgreSQL
    if owl_type == str or owl_type == int or owl_type == float:
        return "VARCHAR(255)" if owl_type == str else "INTEGER" if owl_type == int else "REAL"
    # Agrega más mapeos según sea necesario

def get_property_name(property):
    return property.name if hasattr(property, "name") else str(property)

def get_subproperties(property):
    # Obtener subpropiedades de una propiedad
    return [get_property_name(sub_prop) for sub_prop in property.is_a if sub_prop in onto.data_properties()]

def get_primary_key_property(clase):
    cod_property_name = f'Cod{clase.name.capitalize()}'
    print('nombre PRIMARY KEY', cod_property_name)
    return cod_property_name

for clase in onto.classes():
        print(f"\n-- Tabla para la clase: {clase.name}")

        # Obtener las data properties asociadas a la clase
        data_properties = [(prop.name, get_sql_type(prop.range[0])) for prop in onto.data_properties() if prop.domain[0] == clase]
        
        # Obtener las subpropiedades de las data properties
        sub_properties = []
        for prop in onto.data_properties():
            print(get_subproperties(prop))
            sub_properties.extend(get_subproperties(prop))
        
        # Eliminar duplicados y las propiedades principales que ya están presentes
        sub_properties = list(set(sub_properties) - set(prop[0] for prop in data_properties))
        # Obtener la propiedad principal (PRIMARY KEY) de la clase
        primary_key_prop = get_primary_key_property(clase)
        primary_key = get_property_name(primary_key_prop) if primary_key_prop else None