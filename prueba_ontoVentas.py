#Utilizando libreria owlready2 para ontologias
from owlready2 import get_ontology
from owlready2 import *
#pip install psycopg2 
import psycopg2
import psycopg2.extras


#Conexion con PostgreSQL
DB_HOST = "localhost"
DB_NAME = "db_ventas"
DB_USER = "postgres"
DB_PASS = "12345"

conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"
# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

def crear_tabla():
    print("Generación de Sentencias SQL para Crear Tablas:")
    for clase in onto.classes():
        print(f"\n-- Tabla para la clase: {clase.name}")
        
        # Obtener las data properties asociadas a la clase
        data_properties = [prop.name for prop in onto.data_properties() if prop.domain[0] == clase]

        # Crear la sentencia SQL
        c = f'''
            CREATE TABLE {clase.name} (
                {', '.join([f'{atributo} VARCHAR(255)' for atributo in data_properties])},
                PRIMARY KEY ({data_properties[0]})
            );
        '''
        with conn as conexion:
            with conexion.cursor() as cursor:
                cursor.execute(c)
        
        conexion.commit()
#Solo deberia ejecutarse una vez para crear las tablas usando la ontologia      
crear_tabla()