from owlready2 import get_ontology

# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontoVentas.owx"

# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

# Diccionario para almacenar las subpropiedades de cada propiedad de datos
properties_dict = {}

# Recorre las Data Properties
for data_prop in onto.data_properties():
    # Obtiene todas las subpropiedades (subclases) de la propiedad de datos actual
    sub_properties = [sub_prop.name for sub_prop in data_prop.subclasses() if sub_prop != data_prop]
    
    # Agrega la propiedad de datos y sus subpropiedades al diccionario
    if sub_properties:
        properties_dict[data_prop.name] = sub_properties

# Imprime el diccionario en el formato deseado
print(properties_dict)

